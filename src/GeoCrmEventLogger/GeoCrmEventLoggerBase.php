<?php

namespace GeoCrmEventLogger;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

class GeoCrmEventLoggerBase
{
    protected $log;
    /**
     * Init Log System.
     *
     */
    public function init($child_namespace, $parent_class)
    {
        $logger_name = str_replace($child_namespace .'\\', '', $parent_class);
        $logger_name = str_replace('Logger', '', $logger_name);

        $event_log_dir = env('EVENT_LOG_DIR', '../event_log');
        $log_path =  $event_log_dir  . '/' . str_replace('App\Listeners\\', '', $child_namespace) . '/' . $logger_name . '/.log';

        $this->log = new Logger($logger_name);
        $this->log->pushHandler(new RotatingFileHandler($log_path));
    }
}